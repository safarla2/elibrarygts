﻿using System.Collections.Generic;

namespace eLibraryGTS.Models
{
    public class Person
    {
        public int Id { get; set; }
        public string Name { get; set; }

    }
}
