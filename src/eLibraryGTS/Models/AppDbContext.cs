﻿using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Data.Entity;

namespace eLibraryGTS.Models
{
    public class AppUser : IdentityUser{ }
    public class AppDbContext:IdentityDbContext<AppUser>

    {

        protected override void OnModelCreating(ModelBuilder builder)
             {
            base.OnModelCreating(builder);
        }

        public DbSet<Item> Items { get; set; }
    }
}
