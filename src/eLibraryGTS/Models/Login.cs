﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace eLibraryGTS.Models
{
    public class Login
    {
        [Required]
        [EmailAddress]
        public string EmailAddress { get; set;  }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}
