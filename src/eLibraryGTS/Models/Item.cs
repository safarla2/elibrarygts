﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace eLibraryGTS.Models
{
    [Table("dbo.Items")]
    public class Item
    {
        public int ItemId { get; set; }
        public string Title { get; set; }
        public DateTime Published { get; set; }
        public string Author { get; set; }
    }

   
}
