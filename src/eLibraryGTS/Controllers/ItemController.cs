﻿using Microsoft.AspNet.Mvc;
using System.Threading.Tasks;
using eLibraryGTS.Models;
using Microsoft.Data.Entity;
using System.Collections.Generic;
using System.Linq;




// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace eLibraryGTS.Controllers
{
    public class ItemController : Controller
    {
        //    public AppDbContext DbContext { get; }
        // GET: /<controller>/
        public IActionResult Index()
        {
            using (AppDbContext DbContext = new AppDbContext())
            {
                List<Item> data = DbContext.Items.ToList();
                return View(data);

            }
        }
    }
        

    }

